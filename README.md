<<<<<<< HEAD

This fork of log4j1 was made because the main log4j code line is no longer being maintained and has not since around May 4 2012

This fork enables performance fixes in the way logging happens when under concurrent load.  Troubleshooting has revealed that
logging appenders can become very contended when the system generates a lot of concurrent logging, typically during times when the application is
in trouble, which is exactly when you want your logging system to respond in a timely manner.

Although many of these problems have been fixed in log4j2 (or log-back say) the amount of effort needed to move off log4j1 is enormous.  This fork
is a response to that challenge.

Since this is Apache code it is copyright them and is covered by their ASF licence

http://www.apache.org/licenses/LICENSE-2.0

=======
A fork of log4j1 that improves performance.

log4j1 is effectively dead, but getting Atlassian software off log4j1 is herculean effort.  

In the mean time we can improve some of the performance hurdles in an other wise sturdy library that has served us so well.

The last release of was 1.2.17 and hence master starts from there in terms of the upstream repo.

Since log4j1 is no longer maintained, no upstream changes are expected to be seen nor do we expect to push changes back to Apache since its not going to be released.
>>>>>>> origin/master
